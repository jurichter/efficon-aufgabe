<?php
    require_once 'classes/JobReader.php';
    $jobReader = new JobReader();
    $jobs = $jobReader->readJobs('data\jobs.json');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test für Praktikum</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS Files-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <!-- JS Files -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body id="jobs">
    <div class="content container">
        <div class="row"
            <div class="col-lg-3 logo">
                <img src="images/efficon-sub-icon2.png" alt="logo efficon" />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 slogan">
                Unsere Region. Deine Zukunft.
            </div>
        </div>
        <div class="row">
            <h1>Unsere Stellenangebote</h1>
        </div>
        <div class="row">
            <?php
        foreach($jobs as $job){
            echo '

            <div class="col-xs-12 col-md-4">
                <div class="card">
                    <div class=card-header>'.$job->getTitle().'</div>
                    <div class=card-body>
                    <div class=card-text>'.$job->getDescription().' ...</div></div>
                    <div class=card-footer>
                        <div class=card-subtitle>Ort: '.$job->getCity().'</div>
                        <div class=card-subtitle>Gehalt: '.$job->getMinWage().' - '.$job->getMaxWage().'</div>
                    </div>
                </div>
            </div>
            ';
        }
        ?>
    </div>
</body>
</html>
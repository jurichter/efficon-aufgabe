<?php

require 'File.php';
require 'JobOffer.php';

/**
 * Utilizes File class to read jobs from json file
 * Class JobReader
 */
class JobReader{

    private $jobs;

    /**
     * Reads the Jobs from the given path
     * @param $path
     * @return Array of JobOffer
     */
    public function readJobs($path){
            // Wandelt die JSON in ein Array um.
        $joblist = file_get_contents($path);
        $obj = json_decode($joblist, true);
            // Filtert die gewünschten Werte aus der JSON.
        foreach ($obj as $value) {
            $title = $value['title'];
            $description = $value['description'];
            $minValue = $value['baseSalary']['value']['minValue'];
            $maxValue = $value['baseSalary']['value']['maxValue'];
            $city = $value['jobLocation']['address']['addressLocality'];
            // Übergebe Variablen an eine neue JobOffer-Funktion.
            $this->jobs[] = new JobOffer($title, $description, $minValue, $maxValue, $city);
        }
        return $this->jobs;
    }

    /**
     * Gets the average wage for the attribute job
     * @return string
     */
    public function getAverageWageOfJobs(){

        $func = function($job){
            return array("minValue" => $job->getMinWage(), "maxValue" => $job->getMaxWage());
        };

        return $this->calcAvgWage(array_map($func, $this->jobs)) . "&euro;";
    }

    /**
     * calculates the average wage for the given array
     * @param $baseSalaries
     * @return float|int
     */
    public function calcAvgWage($baseSalaries){
        $sum = 0;
        foreach($baseSalaries as $baseSalary){
            $sum += ($baseSalary['minValue'] + $baseSalary['maxValue']) / 2; 
        }

        return $sum / count($baseSalaries);
    }
}
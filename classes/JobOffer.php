<?php


class JobOffer{
   private $Title;
   private $Description;
   private $minWage;
   private $maxWage;
   private $City;

            /* Bauplan für die Initialisierung gleicht die Werte mit dem Ergebnis
               der JSON-Abfrage bzw. Filterung ab. */
   public function __construct($title, $description, $minValue, $maxValue, $city) {
      $this->Title = $title;
      $this->Description = $description;
      $this->minWage = $minValue;
      $this->maxWage = $maxValue;
      $this->City = $city;

            // Kürzt einen Array am letzten Wort vor 200 Zeichen weg.
      $this->Description = wordwrap($this->Description, 200);
      $this->Description = explode("\n", $this->Description);
      $this->Description = $this->Description[0];
   }
            // Ermöglicht den Zugriff auf die einzelnen Werte.
   public function getTitle(){ return $this->Title; }
   public function getDescription(){ return $this->Description; }
   public function getMinWage(){ return $this->minWage; }
   public function getMaxWage(){ return $this->maxWage; }
   public function getCity(){ return $this->City; }
}